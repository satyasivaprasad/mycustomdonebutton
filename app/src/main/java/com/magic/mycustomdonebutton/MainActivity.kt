package com.magic.mycustomdonebutton

import android.graphics.drawable.AnimatedVectorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var avd: AnimatedVectorDrawableCompat? = null
    var avd2: AnimatedVectorDrawable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        done_button.setOnClickListener {
            val drawable = finish_img.drawable
            if (drawable is AnimatedVectorDrawableCompat) {
                avd  = drawable
                avd!!.start()
            } else {
                avd2 =  drawable as AnimatedVectorDrawable
                avd2!!.start()
            }
        }
    }
}